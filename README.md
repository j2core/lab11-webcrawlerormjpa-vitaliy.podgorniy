# README #

We need to migrate our WebCrawler from Hibernate specific implementation to plain JPA. This will allow us to use any other JPA compatible framework without any code change. 
fork Lab 10 project
migrate everything to plain JPA and use Hibernate as JPA-compatible framework
use JPQL instead of HQL
avoid Hibernate annotations, use only JPA annotations
make sure that project functionality is not changed and multi-threading works fine
(optional) try to use another JPA framework instead of Hibernate